// THIS IS DECOMPILED PROPRIETARY CODE - USE AT YOUR OWN RISK.
//
// The original code belongs to Daisuke "Pixel" Amaya.
//
// Modifications and custom code are under the MIT licence.
// See LICENCE.txt for details.

#pragma once

#include "WindowsWrapper.h"
#define CARET_MAX 0x800//0x40
struct CARET
{
    int cond;
    int code;
    int direct;
    int x;
    int y;
    int xm;
    int ym;
    int act_no;
    int act_wait;
    int ani_no;
    int ani_wait;
    int view_left;
    int view_top;
    int lifetime;
    RECT rect;
};
enum
{
	CARET_NULL = 0,
	CARET_BUBBLE = 1,
	CARET_PROJECTILE_DISSIPATION = 2,
	CARET_SHOOT = 3,
	CARET_SNAKE_AFTERIMAGE = 4,
	CARET_ZZZ = 5,
	CARET_SNAKE_AFTERIMAGE_DUPLICATE = 6,
	CARET_EXHAUST = 7,
	CARET_DROWNED_QUOTE = 8,
	CARET_QUESTION_MARK = 9,
	CARET_LEVEL_UP = 10,
	CARET_HURT_PARTICLES = 11,
	CARET_EXPLOSION = 12,
	CARET_TINY_PARTICLES = 13,
	CARET_UNKNOWN = 14,
	CARET_PROJECTILE_DISSIPATION_TINY = 15,
	CARET_EMPTY = 16,
	CARET_PUSH_JUMP_KEY = 17,
	CARET_AMMO = 18,
};
extern int carets_added_in_tick;
extern CARET gCrt[CARET_MAX];
void InitCaret(void);
void ActCaret(void);
void PutCaret(int fx, int fy);
void SetCaretVel(int x, int y, int code, int ym, int xm, int lifetime);
void SetCaret(int x, int y, int code, int dir);
