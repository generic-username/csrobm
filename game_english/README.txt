== [ Cave Story Simple Double Jump Game ] ==
FAQ:
 Q: My game is lagging.
 A: Get a better computer. Also not a question.

 Q: Did you make this before modfest?
 A: yeah i made a fair bit before modfest whoops lol

 Q: How do I double jump?
 A: Press the jump key while in the air

 Q: Is this CSE2?
 A: CSE2??? Never heard of it.

 Q: This is too hard
 A: not a question.

 Q: Where is the Z key located on my keyboard?
 A: On the standard US-International QUERTY standard keyboard, the Z key is the bottom most left key of the alphabet. The "Shift", "Ctrl", and "Windows" (Also called Mod4), as well as "Alt" are not part of the alphabet.
 A2: Exactly 1.4 inches right, 0.9 inches up from the corner of your keyboard.

== [ WARNING ] ==
This mod contains the "f word"

== [ TERMS OF SERVICE ] ==
By using this software, you agree to NEVER rename 'cowancy_mod_loader.dll' to 'image.png'

You will also not change the file extension on anything in 'mods' to '.mp4'

== [ LICENSE ] ==
Copy and use any of my assets with or without permission

== [ CREDITS ] ==
Studio Gato - Gato
2dbro - orgs

== [ SOURCE ] ==
Cannot be shared on doukutsu club. However, on an unrelated note, check my gitlab account:
https://gitlab.com/generic-username
